var schema = schema; // read from src
var form = form;

for (var modelName in schema.association) {
    var links = schema.association[modelName];
    for (var i = 0; i < links.length; i++) {
        var linkType = links[i][0];
        var linkModelName = links[i][1];
        var replacement = links[i][2];
        if (linkType === schema.C.HAS_ONE_LINK_OF) {
            schema.models[modelName][replacement?replacement.toLowerCase():linkModelName.toLowerCase() + '_id'] = 'number';
        }
    }
}

window.ORM = {};
window.ORM.models = schema.models;
window.ORM.form = form;
