
var deservedKeywords = ["id", "createdAt", "updatedAt", "serialNumber"];

var excludeType = ["date","enum"];
var isAss = function (key) {
    return /^.*_id$/.test(key);
};
var isDK = function (key) {
    for (var i = 0; i < deservedKeywords.length; i++) {
        if (deservedKeywords[i] === key) return true;
    }
    return false;
};
var isET = function (value) {
    for (var i = 0; i < excludeType.length; i++) {
        if (value.type === excludeType[i]) {
            return true;
        }
    }    
    return false;
};

module.exports = {
    HAS_ONE_LINK_OF: 'hasOne',
    HAS_MANY_LINKS_OF: 'hasMany',
    isAss: isAss,
    isDK : isDK,
    isEncryptedColumn: function (key, model) {
        return key && model && !isDK(key) && !isAss(key) && !isET(model);
    }
};
