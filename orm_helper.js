
// TODO : 어떤 DB를 선택하든지 상관없는 구조로 orm을 변경해야 한다.

var browserify = require('browserify');
var fs = require('fs');

var Crypto = require('cryptojs').Crypto;
var encryptKeyword = "__encrypt__"; // encrypt된 데이터임을 눈으로 볼 수 있도록..

var _ = require('lodash');
var p = require('path');
var z = require('zeninode')();
var orm = require('orm');
var async = require('async');

function safe_require(path, exitMsgOrInitVal) {
	try {
		return require(path);
	} catch(e) {
		if(typeof exitMsgOrInitVal === "string") {
			z.p(exitMsgOrInitVal);
			process.exit(-1);
		} else {
			return exitMsgOrInitVal;
		}
	}
}

///////////////////////////////////////////////////////////
// helper class
var ColumnParser = function (schema, columnName, originModel) {
	this.originModel = originModel;
	this.columnName = columnName;

	var joinIndex = columnName.indexOf(".");
	if (joinIndex > -1) {
		this.isJoinCase = true;
		this.joinTableName = columnName.substr(0, joinIndex);
		this.joinColumnName = columnName.substr(joinIndex + 1);
		this.encrypted = true;
	} else {
		this.isJoinCase = false;
		this.encrypted = schema.C.isEncryptedColumn(this.columnName, this.originModel.properties[this.columnName]);
	}
};

ColumnParser.prototype.getValue = function (data) {
	var ret = null;
	if (this.isJoinCase) {
		if (data[this.joinTableName]) {
			if (data[this.joinTableName].constructor.name === "Array") {
				var currData = [];
				for (var i = 0; i < data[this.joinTableName].length; i++) {
					if (data[this.joinTableName][i][this.joinColumnName]) {
						currData.push(data[this.joinTableName][i][this.joinColumnName]);
					}
				}
				ret = currData.join(", ");

			} else {
				ret = data[this.joinTableName][this.joinColumnName];
			}
		} else {
			ret = "--";
		}
	} else {
		ret = data[this.columnName];
	}
	return ret === null ? "" : ret;
};

ColumnParser.prototype.compareAsce = function (a, b) {
	var aValue = this.getValue(a);
	var bValue = this.getValue(b);
	if (aValue < bValue) return -1;
	else if (bValue < aValue) return 1;
	return 0;
};

ColumnParser.prototype.compareDesc = function (a, b) {
	var aValue = this.getValue(a);
	var bValue = this.getValue(b);
	if (aValue > bValue) return -1;
	else if (bValue > aValue) return 1;
	return 0;
};

//////////////////////////////////////////////////
// Connector Class
//
var Connector = function(O, database, cb) {
	this.O = O;
	this.database = database;
	try {
		this.option = require(this.O.config.dbOptionPath)(this);
	} catch(e) {
		this.option = {};
	}

	var _this = this;
	orm.connect(_.assign(this.O.config.db, {database:database}), function (err, db) {
		if(err) {
			cb(err);
		} else {
			_this.models = _this.processModel(db);
			cb(null, _this);
		}
	});
};

////////////////////////////////////////////////////////
// About Encription
//
Connector.prototype.encryptObject = function (modelAttributes, object) {
	if(this.O.config.encryptKey) {
		for (var key in modelAttributes) {
			var type = modelAttributes[key];
			if (this.O.schema.C.isEncryptedColumn(key, type) && object[key]) {
				object[key] = encryptKeyword + Crypto.AES.encrypt(object[key].toString(), this.O.config.encryptKey);
			}
		}
	}
};

// model에 정해진 property들 중에서 암호화할 것들을 암호화한다
// association을 고려하지 않고 model만을 기준으로 해도 하나의 모델에서 저장할 것들은 모두 암호화가 된다.
Connector.prototype.encrypt = function (model, object) {
	if (object === null) return object;
	if (Object.prototype.toString.call(object) === '[object Array]') {
		for (var i = 0; i < object.length; i++) {
			this.encryptObject(model.properties, object[i]);
		}
	} else {
		this.encryptObject(model.properties, object);
	}
	return object;
};

Connector.prototype.decryptObject = function (modelAttributes, modelData) {
	if(this.O.config.encryptKey) {
		for (var field in modelAttributes) {
			if (!this.O.schema.C.isDK(field)) {
				var value = modelData[field];
				if (value) {
					var valueType = value.constructor.name;
					if (valueType === 'Array' || valueType === 'Object') {
						this.decrypt(this.models[modelAttributes[field]], value); // 원래는 type값인데 association의 경우 해당 model name값이 들어있다.
					} else if (valueType === 'String' && value.length > 11 && value.substring(0, 11) === encryptKeyword) {
						modelData[field] = Crypto.AES.decrypt(value.substring(11), this.O.config.encryptKey);
					}
				}
			}
		}
	}
};

Connector.prototype.getAssociationFields = function (model) {
	var fields = {};
	var links = this.O.schema.association[model.table];
	if (links) {
		for (var i = 0; i < links.length; i++) {
			var linkModelName = links[i][1];
			var replacement = links[i][2];
			fields[replacement ? replacement : linkModelName] = linkModelName; // type값으로 modelName을 넣어주어 decrypte에서 활용
		}
	}
	return fields;
};

Connector.prototype.decrypt = function (model, object) {
	if (object === null) return object;
	var allFields = _.extend({}, model.properties, this.getAssociationFields(model));
	if (object.constructor.name === 'Array') {
		for (var i = 0; i < object.length; i++) {
			this.decryptObject(allFields, object[i]);
		}
	} else {
		this.decryptObject(allFields, object);
	}
	return object;
};

//////////////////////////////////////////////////////////
// make initial data
//
Connector.prototype.makeInitialData = function (serverModels, modelName) {
	if (this.O.initialData && this.O.initialData[modelName]) {
		var _this = this;
		var model = serverModels[modelName];
		model.count(function (err, c) {
			if (c === 0) {
				var currData = _this.O.initialData[modelName];
				for (var i = 0; i < currData.length; i++) {
					model.create(_this.encrypt(model, currData[i]), function (err, creating) {
						_this.errHandle(err);
					});
				}
			}
		});
	}
};

//////////////////////////////////////////////////
// database model generation
//

Connector.prototype.updateSearchEngine = function(obj, addOrRemove, cb) {
	if(this.O.search_helper) {
		var dataObj = this.ormInstanceToObj(obj, true);
		var modelName = obj.__opts.table;
		var decyptedData = this.decrypt(this.models[modelName], dataObj);
		if(addOrRemove === 'add') {
			this.O.search_helper.add(this.database, modelName, decyptedData, function(err, res){
				if(err) {
					// TODO mail로 알람을 해줘야한다!!
				}
				if(cb) cb();
			});
		} else if(addOrRemove === 'remove') {
			this.O.search_helper.remove(this.database, modelName, decyptedData, function(err, res){
				if(err) {
					// TODO mail로 알람을 해줘야한다!!
				}
				if(cb) cb();
			});
		}
	}
};

Connector.prototype.mergeHook = function(basicHook, prevHook) {
	if(prevHook) {
		return function(next) {
			if(typeof next === 'function') {
				basicHook.call(this, function(){
					prevHook.call(this, next);
				});
			} else {
				basicHook.call(this, next);
				prevHook.call(this, next);
			}
		};
	} else {
		return basicHook;
	}
};

Connector.prototype.mergeBasicOptions = function (dest) {
	var _this = this;
	if(!dest) dest = {};
	if(!dest.hooks) dest.hooks = {};

	dest.hooks.beforeCreate = _this.mergeHook(function (next) {
		this.createdAt = new Date();
		this.deleted = false;
		return next();
	}, dest.hooks.beforeCreate);

	dest.hooks.beforeSave = _this.mergeHook(function (next) {
		this.updatedAt = new Date();
		return next();
	}, dest.hooks.beforeSave);

	dest.hooks.afterCreate = _this.mergeHook(function (success) {
		if(success) {
			_this.updateSearchEngine(this, "add");
		}
	}, dest.hooks.afterCreate);

	dest.hooks.afterSave = _this.mergeHook(function (success) {
		if(success) {
			_this.updateSearchEngine(this, "add");
		}
	}, dest.hooks.afterSave);

	dest.hooks.afterRemove = _this.mergeHook(function (success) {
		if(success) {
			_this.updateSearchEngine(this, "remove");
		}
	}, dest.hooks.afterSave);

	return dest;
};

Connector.prototype.processModel = function (db) {
	// process for server
	var schemaModels = this.O.schema.models;
	var serverModels = {};

	for (var modelName in schemaModels) {
		serverModels[modelName] = db.define(modelName, _.extend(schemaModels[modelName], {
			createdAt: 'date',
			updatedAt: 'date',
			deleted: 'boolean'
		}), this.mergeBasicOptions(this.option[modelName]));
	}

	// associations
	for (var modelName in this.O.schema.association) {
		var links = this.O.schema.association[modelName];
		for (var i = 0; i < links.length; i++) {
			var linkType = links[i][0];
			var linkModelName = links[i][1];
			var replacement = links[i][2];
			var functionName = linkType === this.O.schema.C.HAS_MANY_LINKS_OF ? "hasMany" : "hasOne";
			serverModels[modelName][functionName](replacement ? replacement : linkModelName, serverModels[linkModelName]);
		}
	}

	var _this = this;
	// sync and create initial data
	for (var modelName in schemaModels) {
		serverModels[modelName].sync(function (modelName) {
			return function (err) {
				if (!_this.errHandle(err)) {
					_this.makeInitialData(serverModels, modelName);
				}
			};
		}(modelName));
	}
	return serverModels;
};

//////////////////////////////////////////////////////
// ETC helpers
//
Connector.prototype.errHandle = function (err) {
	if (err) {
		z.e(err);
		return true;
	} else {
		return false;
	}
};

Connector.prototype.ormInstanceToObj = function(ormInstance, forIndex) {
	var keyMap = Object.keys(ormInstance);
	var doc = {};
	for(var i=0;i<keyMap.length;i++) {
		doc[keyMap[i]] = ormInstance[keyMap[i]];
	}
	if(forIndex) {
		delete doc.password;
		doc.id = doc._id;
		delete doc._id;
	}
	return doc;
};

Connector.prototype.getRule = function(modelName) {
	var database = this.database;
	return _.forEach(this.O.form[modelName].rules, function(rule){
		if(rule.remote && rule.remote.indexOf('/'+database)!==0) {
			rule.remote = "/"+database+rule.remote;
		}
	});
};

//////////////////////////////////////////////////////
// orm wrapper
//
Connector.prototype.create = function(modelName, data, cb) {
	var _this = this;
	this.models[modelName].create(this.encrypt(this.models[modelName], data), cb);
};

Connector.prototype.get = function(modelName, id, cb) {
	var _this = this;
	this.models[modelName].get(id, function(err, obj){
		if(err) {
			cb(err);
		} else {
			cb(err, _this.decrypt(_this.models[modelName], _this.ormInstanceToObj(obj)));
		}
	});
};

Connector.prototype.find = function(modelName, data, cb) {
	if(this.O.search_helper) {
		if(this.models[modelName] 
		this.O.search_helper.find(this.database, modelName, data, cb);
	} else {
		this.models[modelName].find(data, cb);
	}
};

Connector.prototype.reindex = function(cb) {
	var _this = this;
	if(this.O.search_helper) {
		async.waterfall(z.async.task([
		function(cb) {
			_this.O.search_helper.remove(_this.database, null, null, function(err){
				cb();	
			});
		}, function(cb) {
			var tasks = [];
			for(var modelName in _this.models) {
				z.async.taskInjector(tasks, function(cb, modelName) {
					_this.models[modelName].all(function(err, results){
						var tasks = [];
						for(var i=0;i<results.length;i++) {
							var resultObj = _this.decrypt(_this.models[modelName], _this.ormInstanceToObj(results[i], true));
							z.async.taskInjector(tasks, function(cb, resultObj, modelName){
								_this.O.search_helper.add(_this.database, modelName, resultObj, cb);
							}, resultObj, modelName);
						}
						async.waterfall(tasks, cb);
					});
				}, modelName);
			}
			async.waterfall(tasks, cb);
		}
		]), cb);
	} else {
		cb();
	}
};

//////////////////////////////////////////////////////////////////
// Main ORM class
//
var ORM = function(config) {
   	this.config = config; // 설정 정보
	this.schema = safe_require(config.dbSchemaPath, "Config file must have dbSchemaPath"); // 서버 모델을 완성하는데 사용된 스키마

	this.makeClientCode(config.dbSchemaPath, config.formPath, config.clientSchemaPath);

    this.form = safe_require(config.formPath, null); // 입력폼에 대한 설정
	this.initialData = safe_require(config.initialDataPath, null);

	if(config.encryptKey) {
		if(!config.search) {
			z.e(new Error('encryptKey option needs search option'));
			process.exit(-1);
		}
		this.search_helper = require('./search/search_helper').connect(config.search); // 검색엔진
	}
};

ORM.prototype.createConnector = function (database, cb) {
	new Connector(this, database, cb);
};

ORM.prototype.createColumnParser = function(columnName, originModel) {
	return new ColumnParser(this.schema, columnName, originModel);
};

ORM.prototype.makeClientCode = function (schemaPath, formPath, dest) {
	// copy models from src
	var tempFile = p.normalize(__dirname + '/orm_helper_browser_temp.js');

	var writer = fs.createWriteStream(tempFile);

	if (schemaPath.indexOf("\\") > -1) {
		schemaPath = schemaPath.replace(/\\/gi, "\\\\");
	}

	if (formPath && formPath.indexOf("\\") > -1) {
		formPath = formPath.replace(/\\/gi, "\\\\");
	}

	writer.write("var schema = require('" + schemaPath + "');");
	if(formPath) writer.write("var form = require('" + formPath + "');");

	var srcStream = fs.createReadStream(p.normalize(__dirname + '/orm_helper_browser.js'));
	srcStream.pipe(writer);
	srcStream.on("end", function () {
		// convert file for browser after copy src
		var browserJs = fs.createWriteStream(dest);
		var b = browserify();
		b.add(tempFile);
		var bundle = b.bundle();
		bundle.pipe(browserJs);
		bundle.on("end", function () {
			fs.unlink(tempFile);
		});
	});
};

module.exports =  {
	ORM:ORM,
	const:require('./orm_helper_const.js')
};

