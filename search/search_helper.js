
var z = require('zeninode')();

module.exports = {
	connect: function(config) {
		var driver = null;
		try {
			driver = require("./drivers/{engine}_driver.js".format(config))(config);
		} catch(e) {
			z.e(new Error("There is {engine} driver file.").format(config));
			process.exit(-1);
		}

		var remove = function(index, type, query, cb) {
			driver.remove(index, type, query, cb);
		};

		var add = function(index, type, query, cb) {
			driver.add(index, type, query, cb);
		};

		var find = function(index, type, query, cb) {
			driver.find(index, type, query, cb);
		};

		return {
			remove: remove,
			add: add,
			find: find
		};
	}
};

