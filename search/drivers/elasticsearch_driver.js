
var elastical = require('elastical');
var _ = require('lodash');
var async = require('async');
var z = require('zeninode')();

var Driver = function(esClient) {
	this.client = esClient;
};

Driver.prototype.remove = function(index, type, query, cb) {
	if(type && query) {
		if(query.id) {
			this.client.delete(index, type, query.id, cb);
		} else {
			this.client.search(index, type, query, function(err, results) {
				var tasks = [];
				_.forEach(results, function(result){
					z.async.taskInjector(tasks, function(cb, result) {
						this.client.delete(index, type, result.id);
					}, result);
				});
				async.waterfall(tasks, cb);
			});
		}
	} else if(index) {
		this.client.deleteIndex(index, cb);
	}
};

Driver.prototype.add = function(index, type, query, cb) {
	this.client.index(index, type, query, cb);
};

Driver.prototype.find = function(index, type, query, cb) {
	this.client.search({index:index, type:type, query:{term:query}}, function(err, data){
		cb(err, data ? data.hits:[]);	
	});
};

module.exports = function(config) {
	return new Driver(new elastical.Client(config.host, {port:config.port}));
};

