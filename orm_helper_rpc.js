
/**
 * rpc형식으로 클라이언트가 db를 수정할 수 있는 interface를 제공한다
 *
 * https://www.dropbox.com/s/ahv1ksezb54obww/orm_helper_rpc.jpeg
 */

var async = require('async');
var O = null;
var _ = require('lodash');
var C = require('./orm_helper_const');
var z = require('zeninode')();

/**
 * rcp 에러를 핸들링하는 기본 에러 핸들러
 */
var rpcErrHandle = function (error, callback) {
    if (O.errHandle(error)) {
        callback("error", error);
        return true;
    } else return false;
};

/**
 * tasks 목록을 받아수행하고 callback을 호출함
 */
var rpcDoQueryTask = function (tasks, callback) {
    if (tasks.length > 0) {
        async.waterfall(tasks, function (err, errArr) {
            if (!O.errHandle(err)) {
                if (errArr.length === 0) {
                    callback("success");
                } else {
                    callback("error", errArr);
                }
            } else {
                callback("error", err);
            }
        });
    } else {
        callback("success");
    }
};

/**
 * err가 있는지 확인하여 있으면 array넣는다.
 */
function pushErrIfExist(errArr, key, err) {
	if (O.errHandle(err)) {
		errArr.push({key: key, err: err});
		return true;
	} else {
		return false;
	}
}

/**
 * create시에 필요한 task를 생성한다.
 */
function makeCreateTask(subDataModel, subDataIdArr, setterFunc, key) {
	return function (errArr, creating, callback) {
		if (creating) {
			subDataModel.find({id: subDataIdArr}, function (err, subDatas) {
				if (!pushErrIfExist(errArr, key, err)) {
					creating[setterFunc](subDatas, function (err) {
						pushErrIfExist(errArr, key, err);
						callback(null, errArr, creating);
					});
				} else {
					callback(null, errArr, creating);
				}
			});
		} else {
			callback(null, errArr, creating);
		}
	};
}

/**
 * update시에 필요한 task를 생성한다.
 */
function makeUpdateTask(updating, params, key) {
	return function (errArr, callback) {
		var subDataModel = O.models[params[key].modelName];
		var subDataIdArr = params[key].data;

		if (subDataIdArr.length > 0) {
			subDataModel.find({id: subDataIdArr}, function (err, subDatas) {
				if (!pushErrIfExist(errArr, key, err)) {
					updating["set" + key](subDatas, function (err) {
						pushErrIfExist(errArr, key, err);
						callback(null, errArr);
					});
				} else {
					callback(null, errArr);
				}
			});
		} else {
			updating["remove" + key](function (err) {
				pushErrIfExist(errArr, key, err);
				callback(null, errArr);
			});
		}
	};
}

var rpcMethods = {
    "create": function (model, params, callback) {
		// 1. 만들 데이터를 암호화한다.
        O.encrypt(model, params);

		// 2. 일단 주어진 데이터로 create를 한다.
        var tasks = [function (callback) {
            model.create(params, function (err, creating) {
                if (!O.errHandle(err)) {
                    callback(null, [], creating);
                } else {
                    callback(null, [err], null);
                }
            });
        }];

		// 3. SubData를 설정하는 추가 task들을 넣는다.
        for (var key in params) {
            if (key[0].toUpperCase() == key[0]) { // 첫번째 char가 대문자 SubData관련된 필드이다.
				// 3-1. 서브데이터는 따로 task를 생성하여 설정해준다.
                if (params[key]) {
                    var subDataModel = O.models[params[key].modelName];
                    var subDataIdArr = params[key].data;
                    var setterFunc = "set" + key;
                    if(subDataIdArr.length > 0) {
                        tasks.push(makeCreateTask(subDataModel, subDataIdArr, setterFunc, key));
                    }
                }

				// 3-2. 서브데이터의 경우에는 setSubData에서 설정해야하므로 params에서 삭제해버리고 create를 진행한다.
                delete params[key];
            }
        }

		// 4. 주어진 순서대로 task 실행한다.
        rpcDoQueryTask(tasks, callback);
    },
    "update": function (model, params, callback) {
		// 1. update할 객체를 id로 얻어온다
        model.get(params.id, function (err, updating) {
            if (!rpcErrHandle(err, callback)) {
				// 2. update할 데이터를 암호화한다
                O.encrypt(model, params);

				// 3. sub data를 변경하기 전에 변경상황에 대한 save가 이루어져야 error가 생기지 않는다.
                var tasks = [function (callback) {
                    updating.save(function (err) {
                        var errArr = [];
						pushErrIfExist(errArr, key, err);
                        callback(null, errArr);
                    });
                }];

				// 4. 변경할 데이터 key를 돌면서
                for (var key in params) {
					// 4-1. 서브데이터의 경우
                    if (key[0].toUpperCase() == key[0]) { // 첫번째 char가 대문자면 SubData관련된 필드이다.
                        if (params[key]) {
                            tasks.push(makeUpdateTask(updating, params, key));
                        } else if (params[key] === null) {
                            updating["set" + key](null);
                        }
                    } else if (key !== "id") { // sub data가 아닌 부분중 id를 제외하고, 바로 변경한다.
                        updating[key] = params[key];
                    }
                }

				// 5. 위에 주어진 순서대로 task수행
                rpcDoQueryTask(tasks, callback);
            }
        });
    },
    "destroy": function (model, params, callback) {
		// id로 검색해 있으면 해당 객체를 삭제한다.
        model.get(params.id, function (err, deleting) {
            if (!rpcErrHandle(err, callback)) {
                deleting.remove(function (err) {
                    if (!rpcErrHandle(err, callback)) {
                        callback("success");
                    }
                });
            }
        });
    },
    "search": function (model, params, callback) {
		// 검색 쿼리를 암호화한다.
        O.encrypt(model, params);

		// 암호화한 내용으로 검색을 수행하고
        model.find(params, function (err, searchData) {
            if (!rpcErrHandle(err, callback)) {
				// 결과를 보내는 것은 복호화해서 보낸다
                O.decrypt(model, searchData);
                callback(searchData);
            }
        });
    }
};

/**
 * rpc형식의 req를 받아 res를 생성하는 프로세스. req, res를 받아 처리하는 것이 기본적인 node.js의 route처리 형식이고, 따라서 이 함수를 route에 물려주면 알아서 rpc형식의 데이터를 처리하여 response를 보내준다.
 */
/* ajax로 데이터를 생성,변경,삭제,검색해야하는 경우에만 사용한다.*/
function process(req, res) {
    try {
        rpcMethods[req.params.method](O.models[req.params.service], req.body, function (result, error) {
            if (typeof error === 'undefined') error = null;
            res.json({ "result": result, "error": error, "id": req.body.id });
        });
    } catch (e) {
        z.e(e);
        res.json({ "result": "fail", "error": e, "id": req.body.id }); // bad request
    }
}

/*데이터 전달과 페이지 이동이 같이 일어나는 경우에는 일반적인 route에서 받아 이 함수로 처리한다.*/
function processInternal(service, method, params, cb) {
    try {
        rpcMethods[method](O.models[service], params, function (result, error) {
            if (typeof error === 'undefined') error = null;
			cb({ "result": result, "error": error.track, "id": req.body.id });
        });
    } catch (e) {
        z.e(e);
		cb({ "result": "fail", "error": e, "id": req.body.id});
    }
}


// 외부에서 참조하도록 
module.exports = function (orm_helper) {
    O = orm_helper;
    return {
        process: process,
		processInternal: processInternal
    };
};
